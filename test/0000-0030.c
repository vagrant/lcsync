/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include "testdata.h"
#include "testsync.h"
#include "globals.h"
#include "sync.h"
#include "arg.h"
#include <assert.h>
#include <errno.h>
#include <libgen.h>
#include <string.h>

#define MAXFILESZ 1048576
#define MAXFILES 10
#define MAXDIRS 5
#define DEPTH 2
#define TIMEOUT_SECONDS 20

void log_commandline(int argc, char *argv[])
{
	for (int i = 0; i < argc; i++) {
		test_log("%s ", argv[i]);
	}
	test_log("\n");
}

int main(int _argc, char *_argv[])
{
	(void) _argc; /* unused */
	char prog[] = "lcsync";
	char *src, *dst;
	char **argv;
	char *argv1[] = { prog, "-a", NULL, NULL, NULL };
	int argc1 = sizeof argv1 / sizeof argv1[0] - 1;
	int argc, rc;

	test_name("commandline: (local) recursive directory syncing (trailing slash)");

	/* create source directory tree and files */
	rc = test_createtestdirs(basename(_argv[0]), &src, &dst);
	if (!test_assert(rc == 0, "test_createtestdirs()")) return test_status;
	rc = test_createtesttree(src, MAXFILESZ, MAXFILES, MAXDIRS, DEPTH, 0);
	if (!test_assert(rc == 0, "test_createtesttree()")) goto err_free_src_dst;
	//argv1[2] = src;
	size_t len = strlen(src);
	argv1[2] = malloc(len + 2);
	strcpy(argv1[2], src);
	argv1[2][len] = '/';
	argv1[2][len + 1] = '\0';
	argv1[3] = dst;

	/* sync */

#if 0
	/* XXX; match src and dst match */
	char cmd[128];
	//snprintf(cmd, sizeof cmd, "rsync -avq %s/ %s", src, dst);
	snprintf(cmd, sizeof cmd, "cp -Rp %s/. %s", src, dst);
	test_log("`%s`\n", cmd);
	if (system(cmd)) { test_assert(0, "cmd failed"); goto err_free_src_dst; }
#else
	argc = argc1; argv = argv1;
	log_commandline(argc, argv);
	test_assert(arg_parse(&argc, &argv) == 0, "arg_parse()");
	test_assert(action(&argc, argv) == 0, "action()");
#endif

	/* verify */
	test_verify_dirs(src, dst);

err_free_src_dst:
	free(src); free(dst);
	return test_status;
}
