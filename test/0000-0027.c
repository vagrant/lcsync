/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include "testdata.h"
#include "globals.h"
#include "sync.h"
#include "arg.h"
#include <assert.h>
#include <errno.h>
#include <libgen.h>
#include <string.h>

void log_commandline(int argc, char *argv[])
{
	for (int i = 0; i < argc; i++) {
		test_log("%s ", argv[i]);
	}
	test_log("\n");
}

int main(int _argc, char *_argv[])
{
	(void) _argc; /* unused */
	char prog[] = "lcsync";
	char src[] = "0000-0027.src.tmp.XXXXXX";
	char *dstpath;
	char *dir = NULL;
	char **argv;
	char *argv1[] = { prog, "-b", src, NULL, NULL };
	const size_t sz_s = 1024 * 10 + 13;
	int argc1 = sizeof argv1 / sizeof argv1[0] - 1;
	int argc, fd, rc;

	test_name("commandline: sync two local files, destination is directory");

	/* create test file */
	fd = test_data_file(src, sz_s, TEST_TMP | TEST_RND);
	close(fd);
	/* create destination directory */
	rc = test_createtestdir(basename(_argv[0]), &dir, "dst");
	if (!test_assert(rc == 0, "test_createtestdir()")) return test_status;
	argv1[3] = dir;
	rc = snprintf(NULL, 0, "%s/%s", dir, src) + 1;
	dstpath = malloc(rc);
	rc = snprintf(dstpath, rc, "%s/%s", dir, src);

	/* sync */
	argc = argc1; argv = argv1;
	log_commandline(argc, argv);
	test_assert(arg_parse(&argc, &argv) == 0, "arg_parse()");
	test_assert(action(&argc, argv) == 0, "action()");

	/* verify */
	test_assert(test_file_match(src, dstpath) == 0, "test_file_match(): dst matches src");

	free(dir);
	free(dstpath);
	return test_status;
}
