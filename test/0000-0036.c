/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023-2024 Brett Sheffield <bacs@librecast.net> */

/* test 0036:
 *
 * recursive network sync. One of the files is zero-length.
  */

#include "test.h"
#include "testdata.h"
#include "testsync.h"
#include "testnet.h"
#include "globals.h"
#include "sync.h"
#include "arg.h"
#include "sec.h"
#include <assert.h>
#include <errno.h>
#include <libgen.h>
#include <librecast/mdex.h>
#include <semaphore.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>

#define MAXFILESZ 1048576
#define MAXFILES 5
#define MAXDIRS 5
#define DEPTH 2
#define TIMEOUT_SECONDS 20

sem_t sem;

enum {
	LCSHARE = 0,
	LCSYNC  = 1
};

static void handle_sigchld(int signo, siginfo_t *info, void *context)
{
	(void)signo, (void)info, (void)context;
	sem_post(&sem);
}

void log_commandline(int argc, char *argv[])
{
	for (int i = 0; i < argc; i++) {
		test_log("%s ", argv[i]);
	}
	test_log("\n");
}

void exec_lcshare(char *src)
{
	char prog[] = "lcshare";
	char *argv0[] = { prog, "-v", "-b", "--loopback", NULL, NULL };
	char **argv;
	int argc = sizeof argv0 / sizeof argv0[0] - 1;
	argv = argv0;
	argv0[4] = src;
	log_commandline(argc, argv);
	if (execv("../../src/lcshare", argv) == -1) {
		perror("execv");
	}
	exit(EXIT_FAILURE);
}

void exec_lcsync(char *src, char *dst)
{
	char prog[] = "lcsync";
	char *argv0[] = { prog, "-a", "-v", "-b", "--remote", NULL, NULL, NULL };
	char *sharepath;
	char **argv;
	size_t len;
	int argc = sizeof argv0 / sizeof argv0[0] - 1;

	sharepath = strdup(src);
	argv0[6] = dst;
	argv = argv0;

	/* append trailing slash to src */
	len = strlen(sharepath);
	argv0[5] = realloc(sharepath, len + 2);
	if (!argv0[5]) exit(EXIT_FAILURE);
	argv0[5][len] = '/';
	argv0[5][len + 1] = '\0';

	log_commandline(argc, argv);
	if (execv("../../src/lcsync", argv) == -1) perror("execv");
	free(argv0[5]);
	exit(EXIT_FAILURE);
}

int main(int _argc, char *_argv[])
{
	(void) _argc; /* unused */
	char *src = NULL, *dst = NULL, *wd = NULL, *tmp = NULL;
	char *rsrc = NULL;
	char *rdst = NULL;
	char *sub = NULL;
	char *owd = NULL;
	int rc;

	test_cap_require(CAP_NET_ADMIN);
	test_name("commandline: (remote) recursive directory syncing (zero-length file)");
	test_require_net(TEST_NET_BASIC);

	/* create source directory tree and files */
	rc = test_createtestdirs(basename(_argv[0]), &src, &dst);
	if (!test_assert(rc == 0, "test_createtestdirs()")) return test_status;
	rc = test_createtesttree(src, MAXFILESZ, MAXFILES, MAXDIRS, DEPTH, 0);
	if (!test_assert(rc == 0, "test_createtesttree()")) goto err_free_src_dst;

	owd = getcwd(NULL, 0);
	if (!test_assert(owd != NULL, "getcwd()")) goto err_free_src_dst;

	/* create working directory */
	rc = test_createtestdir(basename(_argv[0]), &wd, "src");
	if (!test_assert(rc == 0, "test_createtestdir() (working directory)")) goto err_free_src_dst;

	/* create empty file in source tree */
	rc = chdir(src);
	if (!test_assert(rc == 0, "chdir() (src directory)")) goto err_free_wd;
#if 1
	/* this is the whole point of this test right here */
	rc = creat("zero_length_file", 0644);
	if (!test_assert(rc != -1, "creat() empty file for test")) goto err_free_wd;
	close(rc);
#endif

	if (sem_init(&sem, 0, 0) == -1) goto err_free_wd;

	/* cd back to test/ directory */
	rc = chdir(owd);
	if (!test_assert(rc == 0, "chdir() (old working directory)")) goto err_free_sem;

	/* create new src directory */
	rc = test_createtestdir(basename(_argv[0]), &tmp, "src");
	rc = snprintf(NULL, 0, "%s/subdir", tmp) + 1;
	sub = malloc(rc);
	if (!test_assert(sub != NULL, "malloc(sub)")) goto err_free_sem;
	snprintf(sub, rc, "%s/subdir", tmp);
	rc = rename(src, sub);
	if (!test_assert(rc == 0, "rename '%s' => '%s'", src, sub)) {
		perror("rename");
		goto err_free_sem;
	}

	/* build absolute path for dst */
	rsrc = realpath(tmp, NULL);
	rdst = realpath(dst, NULL);
	src = tmp;

	/* change into working directory */
	rc = chdir(wd);
	if (!test_assert(rc == 0, "chdir() (working directory)")) goto err_free_sem;

	fprintf(stderr, "src: '%s'\n", src);
	fprintf(stderr, "dst: '%s'\n", dst);
	fprintf(stderr, "rsrc: '%s'\n", rsrc);
	fprintf(stderr, "rdst: '%s'\n", rdst);
	fprintf(stderr, "sub: '%s'\n", sub);

#if 0
	/* XXX; match src and dst match */
	char cmd[128];
	//snprintf(cmd, sizeof cmd, "rsync -avq %s/ %s", rsrc, rdst);
	snprintf(cmd, sizeof cmd, "cp -Rp %s/. %s", rsrc, rdst);
	test_log("`%s`\n", cmd);
	if (system(cmd)) { test_assert(0, "cmd failed"); goto err_free_src_dst; }
#else

	/* first, start lcshare () */
	pid_t pid[2];
	pid[LCSHARE] = fork();
	if (!test_assert(pid[LCSHARE] != -1, "fork [LCSHARE]")) {
		goto err_free_sem;
	}
	else if (pid[LCSHARE] == 0) {
		exec_lcshare(rsrc);
	}

	/* now run lcsync */
	sleep(1); /* give lcshare time to index files */
	pid[LCSYNC] = fork();
	if (!test_assert(pid[LCSYNC] != -1, "fork [LCSYNC]")) {
		goto err_free_sem;
	}
	if (pid[LCSYNC]) {
		/* parent - set timed wait for child process */
		struct sigaction act = {0};
		act.sa_sigaction = &handle_sigchld;
		if (sigaction(SIGCHLD, &act, NULL) == -1) {
			perror("sigaction");
		}
		else {
			struct timespec ts = { .tv_sec = TIMEOUT_SECONDS };
			clock_gettime(CLOCK_REALTIME, &ts);
			ts.tv_sec += TIMEOUT_SECONDS;
			if (!test_assert(sem_timedwait(&sem, &ts) == 0 || errno == EINTR, "timeout")) {
				perror("sem_timedwait");
			}
		}
		/* now signal lcshare to stop */
		test_log("sending SIGINT to lcshare\n");
		kill(pid[LCSHARE], SIGINT);
	}
	else {
		exec_lcsync(sub, rdst);
	}
	if (test_status != TEST_OK) goto err_free_sem;

#endif

	/* verify */
	rc = chdir(owd);
	if (!test_assert(rc == 0, "chdir(%s)", rsrc)) goto err_free_sem;
	free(owd);
	test_verify_dirs(sub, rdst);

err_free_sem:
	sem_destroy(&sem);
err_free_wd:
	free(wd);
err_free_src_dst:
	free(rsrc); free(rdst);
	free(src); free(dst);
	return test_status;
}
