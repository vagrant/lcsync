/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022-2023 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include <ifaddrs.h>
#include <limits.h>
#include <net/if.h>
#include <sys/types.h>

/* levels of network availability. Each level requires the previous */
enum {
	TEST_NET_NONE = 0, /* no network interfaces detected */
	TEST_NET_IFACE = 1,/* at least one interface detected */
	TEST_NET_UP = 2,   /* at least one interface is up */
	TEST_NET_ADDR4 = 4, /* IPv4 network address found */
	TEST_NET_ADDR6 = 8, /* IPv6 network address found */
	TEST_NET_MCAST = 16,/* device with MULTICAST flag set found */
	TEST_NET_MROUTE = 32 /* has multicast routes */
};

#define TEST_NET_BASIC TEST_NET_IFACE | TEST_NET_UP | TEST_NET_ADDR6 | TEST_NET_MCAST | TEST_NET_MROUTE

/* find an invalid interface index */
unsigned int get_invalid_ifx(void);

/* find an interface that supports multicast */
unsigned get_multicast_if(void);

/* return level of network available */
int test_net_level(void);

/* exit test with TEST_WARN unless network level is met */
int test_require_net(int required);
