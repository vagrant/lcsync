.TH lcsync 1 2025-03-08 "lcsync" "IPv6 multicast file sync tool"
.SH NAME
lcsync \- IPv6 multicast file sync tool
.SH SYNOPSIS
.P
.nf
Serve a a single file:
    lcshare [OPTION...] FILENAME

Serve all files below a directory:
    lcshare [OPTION...] DIRECTORY

Sync remote file(s) with local:
    lcsync [OPTION...] REMOTEFILENAME LOCALFILENAME

Sync two local files (Path required. Can be ./):
    lcsync ./LOCALFILE1 ./LOCALFILE2
.fi
.P
.SH DESCRIPTION
lcsync is a pair of tools to sync files over IPv6 multicast or the local filesystem. It
splits the file into blocks, hashes them, and compares them in order to
efficiently transfer a minimal amount of data.
.P
.BR lcshare
is the server-side component which indexes files and listens for MLD multicast
group joins and sends the file and directory block data.
.PP
.BR lcsync
is the client-side component which joins multicast groups to receive files and
directories.

.PP
lcshare (server) Options:
.TP
\fB\-\-loopback\fR
enable multicast loopback (sending host will receive sent data)

.PP
lcsync (client) Options:
.TP
\fB\-a\fR, \fB\-\-archive\fR
set archive options [\-g \-o \-p \-r \-t]
.TP
\fB\-n\fR, \fB\-\-dry\-run\fR
don't copy any data
.TP
\fB\-g\fR, \fB\-\-group\fR
set group on destination
.TP
\fB\-o\fR, \fB\-\-owner\fR
set owner on destination
.TP
\fB\-p\fR, \fB\-\-perms\fR
set permissions on destination
.TP
\fB\-\-remote\fR
source path is remote
.TP
\fB\-t\fR, \fB\-\-times\fR
set modification times on destination

.PP
General Options:
.TP
\fB\-b\fR, \fB\-\-batch\fR
Batch mode. No prompting. NB: if you do not specify a keyfile for encryption,
encryption will be disabled.
.TP
\fB\-\-bwlimit INTEGER\fR
Set send rate limit (bps). An SI prefix of T, G, M or K may be added (eg.
\-\-bwlimit 10M)
.TP
\fB\-\-hex\fR
print file hashes in hex
.TP
\fB\-i\fR, \fB\-\-interface\fR INTERFACE
Set network interface to use (default: all)
.TP
\fB\-\-keyfile keyfile\fR
Read symmetric key from
.IR keyfile ,
which must be the path to a file containing a 128 byte random key. This can be
created with a command like:
.PP
.in +7
dd if=/dev/random of=keyfile count=1 bs=128
.TP
\fB\-\-loglevel INTEGER\fR
set loglevel
.TP
\fB\-r\fR, \fB\-\-recursive\fR
recurse into directories
.TP
\fB\-q\fR, \fB\-\-quiet\fR
shhh - we're hunting wabbits
.TP
\fB\-v\fR, \fB\-\-verbose\fR
increase verbosity
.TP
\fB\-V\fR, \fB\-\-version\fR
display version and exit
.PP
To sync remote files, each file is split into blocks and a merkle tree is built
by hashing the blocks using BLAKE3. On the sending/server side, this tree is
sent on Librecast Channel (IPv6 multicast group) that is formed from the hash of
the filename.  The receiver/client joins this channel, and receives the tree.
If the client already has some data to compare, it builds a merkle tree of the
destination file and uses this to quickly compare which blocks differ. It builds
a bitmap with this information, and then joins the Channel(s) for the block(s)
required which are sent by the server
.PP
Forward Error Correction (FEC) is enabled by default using RaptorQ (RFC 6330)
from the Librecast LCRQ library.
.PP
Symmetric encryption is provided using the XSalsa20 stream cipher from libsodium
with Poly1305 MAC authentication tags. A keyfile can be provided, or a key can
be derived from a user-supplied password.
.PP
There is no unicast communication with the server. There are no requests sent,
and the server can sit behind a firewall which is completely closed to inbound
TCP and UDP traffic.  Instead, the server listens on a raw socket for Multicast
Listener Discovery (MLD2) reports. It compares any MLD multicast group JOINs
against the index it built on startup and finds matches for file (tree) and
blocks. In this way, the server only sends data when at least one client is
subscribed.  If more clients want to download the data, the server need take
no further action.  Thus, the load on the server does not change at all,
regardless of whether there is one client or a billion.
.PP
.SH ENVIRONMENT
.TP
.BR LCSYNC_PASSWORD
the value of LCSYNC_PASSWORD will be used to derive the symmetric encryption key
used for encryption and decryption.
.SH BUGS
If you find one, email
.I bugs@librecast.net
.SH SEE ALSO
.BR rsync (1),
.BR ipv6 (7),
.BR lcrq (7)
.SH AUTHOR
lcsync was written by Brett Sheffield <bacs@librecast.net> and
released under the terms of the GPL-2 or (at your option) GPL-3.
.PP
.I https://librecast.net/lcsync.html
