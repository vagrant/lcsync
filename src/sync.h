/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2023 Brett Sheffield <bacs@librecast.net> */

#ifndef _SYNC_H
#define _SYNC_H 1

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

/* hexdump merkle tree for local file */
int file_dump(int *argc, char *argv[]);

/* sync file src to dst */
int file_sync(int *argc, char *argv[]);

/* share file(s) to network */
int net_share(int *argc, char *argv[]);

/* sync file from network */
int net_sync(int *argc, char *argv[]);

#endif /* _SYNC_H */
