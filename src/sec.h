/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

/* sec.h - security-related functions */

#ifndef _SEC_H
#define _SEC_H 1

/* most functions return 0 on success, -1 on error
 * functions returning a pointer will return NULL on error
 * errno is set to indicate the error */

/* drop root privileges */
int sec_drop_privs(void);

/* derive key from password */
int sec_derive_symm_key(char *password);

/* prompt user for a password */
char *sec_getpassword(void);

/* obtain symmetric encryption key from:
 * - file
 * - derive from password
 */
int sec_get_symm_key(void);

#endif /*_SEC_H */
